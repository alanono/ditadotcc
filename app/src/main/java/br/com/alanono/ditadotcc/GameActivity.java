package br.com.alanono.ditadotcc;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.com.alanono.ditadotcc.model.Palavra;
import br.com.alanono.ditadotcc.model.Partida;
import br.com.alanono.ditadotcc.services.PartidaService;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.Sort;

public class GameActivity extends Activity implements
        TextToSpeech.OnInitListener {

    private TextToSpeech tts;

    public static String ARG_RESULTADOS = "resultados";
    public static String ARG_TEMPO = "tempo";
    public static String ARG_ACERTOS = "acertos";
    public static String ARG_ERROS = "erros";
    public static String ARG_PARTIDA = "partida";
    public static String ARG_PROF = "prof";

    public static String ARG_DIFICULDADE = "difilcudade";
    public static String ARG_TAG = "tag";
    public static String MAIS_ERRADAS = "mais_erradas";

    private int qtdPalavras = 10;
    String tag;
    int dificuldade;
    String partida;
    String prof;

    private static MediaPlayer mediaPlayer;
    private List<Palavra> palavras;
    List<Palavra> correcoes = new ArrayList<Palavra>();
    private int i = 0;
    int j = 0;
    Long acertos = 0L;
    Date inicio;

    List<Integer> imagens;

    EditText wordField;
    LinearLayout gameLayout;
    ProgressBar progressBar;
    ImageView playAgainButton;
    ImageView nextWordButton;
    boolean corrigirAcentuacao;
    boolean exibirAcentuacao;

    AssetFileDescriptor erro;
    AssetFileDescriptor acerto;

    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        tts = new TextToSpeech(this, this);

        realm = Realm.getDefaultInstance();

        wordField = (EditText) findViewById(R.id.wordField);
        gameLayout = (LinearLayout) findViewById(R.id.gameLayout);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        playAgainButton = (ImageView) findViewById(R.id.playAgainButton);
        nextWordButton = (ImageView) findViewById(R.id.nextWord);

        corrigirAcentuacao = getSharedPreferences("config", MODE_PRIVATE).getBoolean("corrigir_acent", false);
        exibirAcentuacao = getSharedPreferences("config", MODE_PRIVATE).getBoolean("exibir_acent", false);


        try {
            tag = getIntent().getExtras().getString(ARG_TAG, "");
            dificuldade = getIntent().getExtras().getInt(ARG_DIFICULDADE);
            partida = getIntent().getExtras().getString(ARG_PARTIDA);
            RealmQuery<Palavra> query = realm.where(Palavra.class);

            if(partida != null){
                PartidaService partidaService = new PartidaService();
                Partida partida = partidaService.get(this.partida);
                palavras = partida.getPalavras();
                qtdPalavras = palavras.size();
                prof = partida.getProf();

            } else if (tag != null && tag.equals(MAIS_ERRADAS)) {
                query.lessThan("score", 0).sort("score");
                palavras = query.findAll();
            } else {
                if (!tag.isEmpty() && !tag.equals("all"))
                    query.equalTo("tags.tagName", tag);


                if(dificuldade == 2){
                    query.greaterThanOrEqualTo("dificuldade", dificuldade);
                    query.sort(new String[]{"dificuldade", "aparicoes"}, new Sort[]{Sort.ASCENDING, Sort.ASCENDING});
                }
                else if(dificuldade == 3){
                    query.lessThanOrEqualTo("dificuldade", dificuldade);
                    query.sort(new String[]{"dificuldade", "aparicoes"}, new Sort[]{Sort.DESCENDING, Sort.ASCENDING});
                }
                else{
                    query.equalTo("dificuldade", dificuldade);
                    query.sort("aparicoes");
                }
                palavras = query.findAll();

            }

            mediaPlayer = new MediaPlayer();

            wordField.requestFocus();

            wordField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    wordField.requestFocus();
                }
            });
            wordField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_NEXT) {
                        nextWord(v);
                    }
                    return true;
                }
            });

            acerto = getAssets().openFd("config_acerto.mp3");
            erro = getAssets().openFd("config_erro.mp3");

            nextWordButton.setEnabled(true);
            nextWordButton.setAlpha(1f);
            inicio = new Date();
            //tocar(resumos.get(i).getFile());
            tts.setSpeechRate(0.8F);
            tts.speak(palavras.get(i).getPalavra(), TextToSpeech.QUEUE_FLUSH, null);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void onResume() {

        super.onResume();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void playAgain(View view) {
        tts.setSpeechRate(0.8F);
        tts.speak(palavras.get(i).getPalavra(), TextToSpeech.QUEUE_FLUSH, null);
    }
    public void playSlowly(View view) {
        tts.setSpeechRate(0.3F);
        tts.speak(palavras.get(i).getPalavra(), TextToSpeech.QUEUE_FLUSH, null);
    }

    public void nextWord(View view) {
        try {
            realm.beginTransaction();

            String input = wordField.getText().toString();
            Palavra p = palavras.get(i);
            boolean acertou = p.corrigir(input, false);
            realm.commitTransaction();
            correcoes.add(p);

            if (acertou) {
                new MeuMediaPlayer(acerto).execute();
                acertos++;
            } else {
                new MeuMediaPlayer(erro).execute();
            }

            i++;
            if (i < palavras.size() && i < qtdPalavras) {
                wordField.setText("");
                //Thread.sleep(1000);
                //tts.speak(resumos.get(i).getPalavra(), TextToSpeech.QUEUE_FLUSH, null);
            } else {

                Date tempo = new Date();
                BaseApp app = (BaseApp) getApplication();
                app.setResult(correcoes);
                Intent intent = new Intent(this, ResultActivity.class);
                intent.putExtra(ARG_TEMPO, tempo.getTime() - inicio.getTime());
                intent.putExtra(ARG_ACERTOS, acertos);
                intent.putExtra(ARG_ERROS, correcoes.size()-acertos);
                intent.putExtra(ARG_DIFICULDADE, dificuldade);
                intent.putExtra(ARG_TAG, tag);
                intent.putExtra(ARG_PARTIDA, partida);
                intent.putExtra(ARG_PROF, prof);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }

        } catch (Exception e) {
            Log.e("", "", e);

        }
    }

    public void tocar(String arquivo) {
        try {
            new MeuMediaPlayer(arquivo == null ? null : getAssets().openFd(arquivo)).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void shake() {
        Animation shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
        gameLayout.startAnimation(shake);
    }

    public void yep() {
        Animation shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.yep);
        gameLayout.startAnimation(shake);
    }

    public void changeBackground(View view) {
//        if (j >= imagens.size()) j = 0;
//        gameLayout.setBackground(getResources().getDrawable(imagens.get(j++)));
    }

    @Override
    public void onDestroy() {
        // Don't forget to shutdown tts!
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

    @Override
    public void onInit(int status) {

        if (status == TextToSpeech.SUCCESS) {
            Locale localeBR = new Locale("pt","br");
            int result = tts.setLanguage(localeBR);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                tts.speak(palavras.get(i).getPalavra(), TextToSpeech.QUEUE_FLUSH, null);
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }

    }

    public class MeuMediaPlayer extends AsyncTask<Void, Void, Void> {

        AssetFileDescriptor fileDescriptor;

        public MeuMediaPlayer(AssetFileDescriptor fileDescriptor) {
            this.fileDescriptor = fileDescriptor;
        }

        @Override
        protected Void doInBackground(Void... a) {
            if(fileDescriptor == null)
                return null;
            synchronized (mediaPlayer) {
                try {
                    mediaPlayer.reset();
                    mediaPlayer.setDataSource(fileDescriptor.getFileDescriptor(), fileDescriptor.getStartOffset(), fileDescriptor.getLength());
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                } catch (IOException io) {
                    io.printStackTrace();
                }
                while (mediaPlayer.isPlaying()) {
                    try {
                        Thread.sleep(70);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }

        }

        @Override
        protected void onPreExecute() {
            playAgainButton.setEnabled(false);
            playAgainButton.setAlpha(0.5f);
            nextWordButton.setEnabled(false);
            nextWordButton.setAlpha(0.5f);
            if (fileDescriptor == acerto)
                yep();
            else if (fileDescriptor == erro)
                shake();

        }

        @Override
        protected void onProgressUpdate(Void... progress) {
            playAgainButton.setEnabled(false);
            playAgainButton.setAlpha(0.5f);
            nextWordButton.setEnabled(false);
            nextWordButton.setAlpha(0.5f);
        }

        @Override
        protected void onPostExecute(Void a) {
            playAgainButton.setEnabled(true);
            playAgainButton.setAlpha(1f);
            nextWordButton.setEnabled(true);
            nextWordButton.setAlpha(1f);
            if(i<palavras.size()){
                tts.setSpeechRate(0.8F);
                tts.speak(palavras.get(i).getPalavra(), TextToSpeech.QUEUE_FLUSH, null);
            }
        }
    }

}