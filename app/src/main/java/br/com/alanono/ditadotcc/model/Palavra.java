package br.com.alanono.ditadotcc.model;

import java.io.Serializable;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by eusoj on 07/03/2018.
 */

public class Palavra extends RealmObject implements Serializable {
    @PrimaryKey
    String palavraId;
    String palavra;
    RealmList<Tag> tags;
    transient boolean acertou;
    transient String input;

    Long acertos;
    Long erros;
    Long dificuldade;
    Long score;
    Long aparicoes;

    public Palavra(String palavraId, String palavra) {
        this.palavraId = palavraId;
        this.palavra = palavra;
    }

    public Palavra() {
    }

    public Palavra(PalavraDTO p) {
        this.acertou = p.acertou;
        this.erros = p.erros;
        this.acertos = p.acertos;
        this.palavra = p.palavra;
        this.palavraId = p.palavraId;
        this.input = p.input;
    }

    public boolean corrigir(String input, boolean corrigirAcentos) {
        this.input = input;
        String a = getPalavra();
        String b = input;
        if(aparicoes == null){
            aparicoes = acertos = score = erros = 0L;
        }
        aparicoes++;
        if (!corrigirAcentos) {
            a = removeAcentos(a);
            b = removeAcentos(b);
        }
        if (a.equalsIgnoreCase(b)) {
            acertos++;
            score++;
            acertou = true;
            return true;
        }
        erros++;
        score--;
        acertou = false;
        return false;

    }

    public static String removeAcentos(String p) {
        String str = p;
        str = Normalizer.normalize(str, Normalizer.Form.NFD);
        str = str.replaceAll("[^\\p{ASCII}]", "");
        return str.toUpperCase();
    }

    public String removeAcentos() {
        String str = getPalavra();
        str = Normalizer.normalize(str, Normalizer.Form.NFD);
        str = str.replaceAll("[^\\p{ASCII}]", "");
        return str.toUpperCase();
    }

    public String getPalavraId() {
        return palavraId;
    }

    public void setPalavraId(String palavraId) {
        this.palavraId = palavraId;
    }

    public String getPalavra() {
        return palavra;
    }

    public void setPalavra(String palavra) {
        this.palavra = palavra;
    }

    public RealmList<Tag> getTags() {
       return tags;
    }

    public void setTags(RealmList<Tag> tags) {
        this.tags = tags;
    }

    public long getAcertos() {
        return acertos;
    }

    public void setAcertos(long acertos) {
        this.acertos = acertos;
    }

    public long getErros() {
        return erros;
    }

    public void setErros(long erros) {
        this.erros = erros;
    }

    public long getDificuldade() {
        return dificuldade;
    }

    public void setDificuldade(long dificuldade) {
        this.dificuldade = dificuldade;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public long getAparicoes() {
        return aparicoes;
    }

    public void setAparicoes(long aparicoes) {
        this.aparicoes = aparicoes;
    }

    public boolean isAcertou() {
        return acertou;
    }

    public void setAcertou(boolean acertou) {
        this.acertou = acertou;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getFile(){
        return getPalavraId() + ".mp3";
    }
}
