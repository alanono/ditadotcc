package br.com.alanono.ditadotcc.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.alanono.ditadotcc.R;
import br.com.alanono.ditadotcc.model.Palavra;


/**
 * Created by Alan on 10/10/2015.
 */
public class ResultadoAdapter extends ArrayAdapter<Palavra> {
    List<Palavra> resultados;
    private Context context;
    private LayoutInflater inflater;

    public ResultadoAdapter(Context context, List<Palavra> resultados) {
        super(context, R.layout.row_resultado, resultados);
        this.resultados = resultados;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.row_resultado, parent, false);
        }

        Palavra r = resultados.get(position);
        TextView palavra = (TextView) convertView.findViewById(R.id.palavra);
        TextView input = (TextView) convertView.findViewById(R.id.input);

        palavra.setText(r.getPalavra());

        if(r.getInput().trim().isEmpty())
            input.setText("X");
        else
            input.setText(r.getInput());

        if(r.isAcertou()){
            input.setTextColor(context.getResources().getColor(R.color.certo));
        }
        else{
            input.setTextColor(context.getResources().getColor(R.color.errado));
        }

        return convertView;
    }
}
