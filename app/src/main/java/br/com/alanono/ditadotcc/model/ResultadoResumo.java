package br.com.alanono.ditadotcc.model;

import java.io.Serializable;

/**
 * Created by eusoj on 07/03/2018.
 */

public class ResultadoResumo implements Serializable {
    String _id;

    Long acertos;
    Long erros;
    Long count;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Long getAcertos() {
        return acertos;
    }

    public void setAcertos(Long acertos) {
        this.acertos = acertos;
    }

    public Long getErros() {
        return erros;
    }

    public void setErros(Long erros) {
        this.erros = erros;
    }

    public Long getCount() {
        return count-1;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public double getMediaAcertos(){
        long t = acertos + erros;
        if(count == 0 || t == 0)
            return 0;
        return acertos*100.0/t;
    }
}
