package br.com.alanono.ditadotcc;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.alanono.ditadotcc.model.Partida;
import br.com.alanono.ditadotcc.model.Usuario;
import br.com.alanono.ditadotcc.services.PalavrasService;
import br.com.alanono.ditadotcc.services.PartidaService;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;


public class LoginFragment extends Fragment {
    @BindView(R.id.loginEt)
    EditText loginEt;

    @BindView(R.id.senhaEt)
    EditText senhaEt;

    BaseApp app;

    final Realm realm = Realm.getDefaultInstance();

    public static String TAG = "LoginFragment";

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);
        app = (BaseApp) getActivity().getApplication();
        String usuarioProf = app.getUsuarioProf();
        if(usuarioProf != null && !usuarioProf.isEmpty()){
            getFragmentManager().beginTransaction().replace(R.id.fragment_container, ProfessorMenuFragment.newInstance()).addToBackStack(null).commit();
        }

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @OnClick(R.id.iniciarPartida)
    public void login() {
        Usuario u = new Usuario();
        u.setLogin(loginEt.getText().toString());
        u.setSenha(senhaEt.getText().toString());
        Gson gson = new Gson();
        String jsonInString = gson.toJson(u);
        JSONObject mJSONObject = null;
        try {
            mJSONObject = new JSONObject(jsonInString);
            Rest.getInstance().doPost("/login", mJSONObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(final JSONObject response) {
                    try {
                        app.setUsuarioProf(response.getString("id"));
                    } catch (JSONException e) {
                        Log.e(TAG, "erro", e);
                    }
                    getFragmentManager().beginTransaction().replace(R.id.fragment_container, ProfessorMenuFragment.newInstance()).addToBackStack(null).commit();

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, error.getMessage(), error.fillInStackTrace());
                    Toast.makeText(getActivity(), "Usuário e/ou senha inválidos", Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e){
            Log.e(TAG, "erro", e);
        }
    }
}
