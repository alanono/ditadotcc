package br.com.alanono.ditadotcc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ScrollView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.List;

import br.com.alanono.ditadotcc.model.Palavra;
import br.com.alanono.ditadotcc.model.Partida;
import br.com.alanono.ditadotcc.services.PalavrasService;
import br.com.alanono.ditadotcc.services.PartidaService;
import io.realm.Realm;

public class MainActivity extends Activity {

    final Realm realm = Realm.getDefaultInstance();

    ScrollView fragmentContainer;
    private int dificuldade;
    private String tag = "";

    public static String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentContainer = (ScrollView) findViewById(R.id.fragment_container);
        getFragmentManager().beginTransaction().replace(R.id.fragment_container, MainFragment.newInstance()).addToBackStack(null).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void openTags(View v) {
        switch (v.getId()){
            case R.id.facil:
                dificuldade = 1;
                break;
            case R.id.medio:
                dificuldade = 2;
                break;
            case R.id.dificil:
                dificuldade = 3;
                break;
            case R.id.muito_dificil:
                dificuldade = 4;
        }
        if (dificuldade == 1 || dificuldade == 4)
            startGame();
        else
            getFragmentManager().beginTransaction().replace(R.id.fragment_container, TagFragment.newInstance()).addToBackStack(null).commit();
    }

    public void startGame(View v) {
        tag = getResources().getResourceEntryName(v.getId());
        startGame();
    }

    public void startGame() {
        Intent game = new Intent(getApplicationContext(), GameActivity.class);
        game.putExtra(GameActivity.ARG_DIFICULDADE, dificuldade);
        game.putExtra(GameActivity.ARG_TAG, tag);
        game.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(game);
    }

    public void openEstatisticas(View view) {
        Intent intent = new Intent(getApplicationContext(), EstatisticasActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    public void openProfessor(View view) {
        PalavrasService palavrasService = new PalavrasService();
        palavrasService.sinc();
        List<Palavra> p = palavrasService.list();
        p.size();
        PartidaService partidaService = new PartidaService();
        //partidaService.getFromServer("ttt", null, null);
        Partida partida = partidaService.get("ttt");
        p = palavrasService.list();
        getFragmentManager().beginTransaction().replace(R.id.fragment_container, LoginFragment.newInstance()).addToBackStack(null).commit();

    }

    public void openIngresso(View view) {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container, IniciarPartidaFragment.newInstance()).addToBackStack(null).commit();

    }

    public void openConfiguracoes(View view) {
/*
        Intent intent = new Intent(getApplicationContext(), ConfiguracoesActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivityForResult(intent, 1);
*/
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 1) {
            getFragmentManager().popBackStack();
            super.onBackPressed();
        } else {
            getFragmentManager().popBackStack();
        }
    }

}
