package br.com.alanono.ditadotcc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import br.com.alanono.ditadotcc.adapters.ResultadoAdapter;
import br.com.alanono.ditadotcc.model.Palavra;
import br.com.alanono.ditadotcc.model.ResultadoPartidaDTO;

public class ResultadosFinalActivity extends Activity {

    public static String ARG_RESULT = "result";
    ResultadoPartidaDTO result;
    ResultadoAdapter resultadoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_result_final);

        BaseApp app = (BaseApp) getApplication();
        result = (ResultadoPartidaDTO) getIntent().getSerializableExtra(ARG_RESULT);

        List<Palavra> palavras = result.getPalavras();
        resultadoAdapter = new ResultadoAdapter(this, palavras);
        GridView gridView = (GridView) findViewById(R.id.grid_result);
        gridView.setAdapter(resultadoAdapter);

        TextView tempo = (TextView) findViewById(R.id.tempo);
        TextView acertos = (TextView) findViewById(R.id.qtdAcerto);
        TextView title = (TextView) findViewById(R.id.title);
        title.setText(result.getUsuario() + " - " + result.getPartidaId());

        SimpleDateFormat format = new SimpleDateFormat("mm:ss");

        tempo.setText(format.format(result.getTime()));
        acertos.setText(result.getAcertos() + "/" + (result.getAcertos() + result.getErros()));


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_result, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void jogarNovamente(View view) {
        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra(GameActivity.ARG_DIFICULDADE, getIntent().getIntExtra(GameActivity.ARG_DIFICULDADE, 1));
        intent.putExtra(GameActivity.ARG_TAG, getIntent().getStringExtra(GameActivity.ARG_TAG));
        intent.putExtra(GameActivity.ARG_PARTIDA, getIntent().getStringExtra(GameActivity.ARG_PARTIDA));
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    public void openMenu(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

}
