package br.com.alanono.ditadotcc.services;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import br.com.alanono.ditadotcc.Rest;
import br.com.alanono.ditadotcc.model.Config;
import br.com.alanono.ditadotcc.model.Palavra;
import io.realm.Realm;
import io.realm.RealmQuery;

/**
 * Created by eusoj on 22/06/2018.
 */

public class PalavrasService {
    String TAG = "PalavrasService";


    public void sinc() {
        final Realm realm = Realm.getDefaultInstance();
        RealmQuery<Config> query = realm.where(Config.class);
        Config config = query.findFirst();
        if(config == null ){
            realm.beginTransaction();
            config = new Config(1L);
            realm.copyToRealmOrUpdate(config);
            realm.commitTransaction();
        }
        final Date inicioSinc = new Date();
        Rest.getInstance().getArray("/palavras/sinc/"+config.getUltimaSincFormatado(), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(final JSONArray response) {
                Log.i(TAG, response.toString());
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.createOrUpdateAllFromJson(Palavra.class, response);
                        RealmQuery<Config> query = realm.where(Config.class);
                        Config config = query.findFirst();
                        config.setUltimaSinc(inicioSinc.getTime());
                    }
                });
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.getMessage(), error.fillInStackTrace());
            }
        });
    }

    public List<Palavra> list(){
        final Realm realm = Realm.getDefaultInstance();
        RealmQuery<Palavra> query = realm.where(Palavra.class);
        return query.findAll();

    }
}