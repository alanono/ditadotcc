package br.com.alanono.ditadotcc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.speech.tts.TextToSpeech;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.alanono.ditadotcc.model.Palavra;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class GeraAudioActivity extends Activity implements TextToSpeech.OnInitListener {

    private TextToSpeech mTts;
    RealmResults<Palavra> r;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Palavra> query = realm.where(Palavra.class);
        r = query.findAll();
        mTts = new TextToSpeech(this, this);
        Intent checkIntent = new Intent();
        checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        //startActivityForResult(checkIntent, 5);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    public void open(View view) {
        Intent checkIntent = new Intent();
        checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkIntent, 5);
    }

    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        if (requestCode == 5) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                // success, create the TTS instance
                mTts = new TextToSpeech(this, this);
            } else {
                // missing data, install it
                Intent installIntent = new Intent();
                installIntent.setAction(
                        TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installIntent);
            }
        }
    }

    @Override
    public void onInit(int i) {
//        String myText1 = "O rato roeu a roupa do rei de roma";
//        String myText2 = "análise, casamento, conclusão, pesquisa, tesoura";
//        mTts.speak(myText1, TextToSpeech.QUEUE_FLUSH, null);
//        mTts.speak(myText2, TextToSpeech.QUEUE_ADD, null);

        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).getPath();
        for(Palavra p : r) {
            String destFileName = path + "/" + p.getPalavraId() + ".wav";
            File file = new File(destFileName);
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            HashMap<String, String> myHashRender = new HashMap();
            myHashRender.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, p.getPalavra());
            mTts.synthesizeToFile(p.getPalavra(), myHashRender, destFileName);
        }
    }
}
