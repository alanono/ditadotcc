package br.com.alanono.ditadotcc;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.alanono.ditadotcc.adapters.ResultadoDetalheAdapter;
import br.com.alanono.ditadotcc.model.ResultadoPartidaDTO;
import butterknife.BindView;
import butterknife.ButterKnife;


public class ListarResultadosDetalhesFragment extends Fragment {

    @BindView(R.id.listPalavras)
    ListView listView;

    @BindView(R.id.partidaId)
    TextView partidaIdEt;

    List<ResultadoPartidaDTO> resumos = new ArrayList<>();

    ResultadoDetalheAdapter adapter;

    String partida;

    public static ListarResultadosDetalhesFragment newInstance(String partida) {
        ListarResultadosDetalhesFragment fragment = new ListarResultadosDetalhesFragment();
        fragment.partida = partida;
        return fragment;
    }

    public ListarResultadosDetalhesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_listar_resultados_detalhes, container, false);
        ButterKnife.bind(this, view);
        BaseApp app = (BaseApp) getActivity().getApplication();
        String usuarioProf = app.getUsuarioProf();
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Carregando dados... aguarde");
        dialog.setIndeterminate(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();

        Rest.getInstance().getArray("/resultados/" + usuarioProf + "/" + partida, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(final JSONArray response) {
                Log.i("", response.toString());
                dialog.cancel();
                Gson gson = new Gson();
                resumos = Arrays.asList(gson.fromJson(response.toString(), ResultadoPartidaDTO[].class));
                adapter = new ResultadoDetalheAdapter(getActivity(), resumos);
                listView.setAdapter(adapter);

                partidaIdEt.setText("Partida: " + resumos.get(0).getPartidaId());

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        ResultadoPartidaDTO r = adapter.getItem(i);
                        Intent intent = new Intent(getActivity(), ResultadosFinalActivity.class);
                        intent.putExtra(ResultadosFinalActivity.ARG_RESULT, r);
                        startActivity(intent);
                    }
                });

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.cancel();
                Log.e("", error.getMessage(), error.fillInStackTrace());
                Toast.makeText(getActivity(), "Partida não encontrada", Toast.LENGTH_LONG).show();
            }
        });


        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }



}
