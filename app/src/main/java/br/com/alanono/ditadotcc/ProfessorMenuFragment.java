package br.com.alanono.ditadotcc;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class ProfessorMenuFragment extends Fragment {

    public static ProfessorMenuFragment newInstance() {
        ProfessorMenuFragment fragment = new ProfessorMenuFragment();
        return fragment;
    }

    public ProfessorMenuFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_menu_professor, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @OnClick(R.id.criarPartida)
    public void criarPartida() {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container, CriarPartidaFragment.newInstance()).addToBackStack(null).commit();
    }

    @OnClick(R.id.consultarResultados)
    public void consultarResultados() {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container, ListarResultadosFragment.newInstance()).addToBackStack(null).commit();
    }

    @OnClick(R.id.voltar)
    public void voltar() {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container, MainFragment.newInstance()).addToBackStack(null).commit();
    }

    @OnClick(R.id.logout)
    public void logout() {
        BaseApp app = (BaseApp) getActivity().getApplication();
        app.logout();
        getFragmentManager().beginTransaction().replace(R.id.fragment_container, MainFragment.newInstance()).addToBackStack(null).commit();
    }

}
