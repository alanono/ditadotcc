package br.com.alanono.ditadotcc.model;

import java.io.Serializable;
import java.text.Normalizer;

import io.realm.RealmList;

/**
 * Created by eusoj on 07/03/2018.
 */

public class Usuario implements Serializable {
    String _id;
    String login;
    String senha;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
