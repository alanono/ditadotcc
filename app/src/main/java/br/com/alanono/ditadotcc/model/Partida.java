package br.com.alanono.ditadotcc.model;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by eusoj on 07/03/2018.
 */

public class Partida extends RealmObject {
    @PrimaryKey
    String partidaId;
    String partida;
    RealmList<Palavra> palavras;
    String prof;

    public String getPartidaId() {
        return partidaId;
    }

    public void setPartidaId(String partidaId) {
        this.partidaId = partidaId;
    }

    public String getPartida() {
        return partida;
    }

    public void setPartida(String partida) {
        this.partida = partida;
    }

    public RealmList<Palavra> getPalavras() {
        return palavras;
    }

    public void setPalavras(RealmList<Palavra> palavras) {
        this.palavras = palavras;
    }

    public String getProf() {
        return prof;
    }

    public void setProf(String prof) {
        this.prof = prof;
    }
}
