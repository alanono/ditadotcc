package br.com.alanono.ditadotcc.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.alanono.ditadotcc.R;
import br.com.alanono.ditadotcc.model.Palavra;


/**
 * Created by Alan on 10/10/2015.
 */
public class EstatisticasAdapter extends ArrayAdapter<Palavra> {
    List<Palavra> palavras;
    private Context context;
    private LayoutInflater inflater;

    public EstatisticasAdapter(Context context, List<Palavra> palavras) {
        super(context, R.layout.row_estatistica, palavras);
        this.palavras = palavras;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.row_estatistica, parent, false);
        }

        Palavra p = palavras.get(position);
        TextView palavra = (TextView) convertView.findViewById(R.id.palavra);
        TextView erros = (TextView) convertView.findViewById(R.id.erros);
        TextView acertos = (TextView) convertView.findViewById(R.id.acertos);

        palavra.setText(p.getPalavra());
        erros.setText(p.getErros()+"");
        acertos.setText(p.getAcertos()+"");

        return convertView;
    }
}
