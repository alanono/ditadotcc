package br.com.alanono.ditadotcc.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by eusoj on 07/03/2018.
 */

public class Config extends RealmObject {
    static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");//2018-06-10T14:16:19.762Z
    @PrimaryKey
    int id = 1;
    Long ultimaSinc;

    String usuario;

    String usuarioProf;

    public Config() {
    }

    public Config(Long ultimaSinc) {
        this.ultimaSinc = ultimaSinc;
    }

    public String getUltimaSincFormatado(){
        return sdf.format(new Date(this.getUltimaSinc()));
    }


    public Long getUltimaSinc() {
        return ultimaSinc;
    }

    public void setUltimaSinc(Long ultimaSinc) {
        this.ultimaSinc = ultimaSinc;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getUsuarioProf() {
        return usuarioProf;
    }

    public void setUsuarioProf(String usuarioProf) {
        this.usuarioProf = usuarioProf;
    }
}
