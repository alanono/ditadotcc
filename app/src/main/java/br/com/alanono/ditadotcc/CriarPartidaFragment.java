package br.com.alanono.ditadotcc;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.alanono.ditadotcc.adapters.ListPalavrasPartidaAdapter;
import br.com.alanono.ditadotcc.adapters.PalavraAutoCompleteAdapter;
import br.com.alanono.ditadotcc.model.Palavra;
import br.com.alanono.ditadotcc.model.Partida;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;


public class CriarPartidaFragment extends Fragment {

    @BindView(R.id.listPalavras)
    ListView listView;

/*
    @BindView(R.id.palavraEt)
    EditText palavraEt;
*/

    @BindView(R.id.acPalavra)
    AutoCompleteTextView acPalavra;

    @BindView(R.id.partidaEt)
    EditText partidaEt;

    List<String> palavras = new ArrayList<String>();

    ListPalavrasPartidaAdapter adapter;

    Realm realm;

    public static CriarPartidaFragment newInstance() {
        CriarPartidaFragment fragment = new CriarPartidaFragment();
        return fragment;
    }

    public CriarPartidaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_criar_partida, container, false);
        ButterKnife.bind(this, view);

        realm = Realm.getDefaultInstance();
        RealmQuery<Palavra> query = realm.where(Palavra.class).sort("palavra");
        List<Palavra> p = query.findAll();
        List<String> ps = new ArrayList<>();
        for(Palavra a: p){
            ps.add(a.getPalavra());
        }

        PalavraAutoCompleteAdapter autoCompleteAdapter = new PalavraAutoCompleteAdapter(getActivity(), android.R.layout.simple_list_item_1, ps);
        acPalavra.setAdapter(autoCompleteAdapter);

        adapter = new ListPalavrasPartidaAdapter(getActivity(), palavras);
        listView.setAdapter(adapter);



        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @OnClick(R.id.criarPartida)
    public void submit() {
       RealmList<Palavra> lp = new RealmList<Palavra>();
       for(String p: palavras){
           lp.add(new Palavra(p, p));
       }
       Partida p = new Partida();
       p.setPartidaId(partidaEt.getText().toString());
       p.setPalavras(lp);
       BaseApp app = (BaseApp) getActivity().getApplication();
       String usuarioProf = app.getUsuarioProf();
       p.setProf(usuarioProf);

       Gson gson = new Gson();
       String jsonInString = gson.toJson(p);
        JSONObject mJSONObject = null;
        try {
            mJSONObject = new JSONObject(jsonInString);
            Rest.getInstance().doPost("/partidas", mJSONObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Toast.makeText(getActivity(), "Partida " + partidaEt.getText().toString() + " criada com sucesso", Toast.LENGTH_LONG).show();
                    getFragmentManager().popBackStack();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getActivity(), "Desculpe, ocorreu um problema", Toast.LENGTH_LONG).show();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @OnClick(R.id.btnAdd)
    public void add() {
/*
        palavras.add(palavraEt.getText().toString());
        palavraEt.setText("");
*/
        palavras.add(acPalavra.getText().toString());
        acPalavra.setText("");

        adapter.notifyDataSetChanged();
    }
}
