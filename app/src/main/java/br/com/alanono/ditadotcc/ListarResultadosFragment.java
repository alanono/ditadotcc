package br.com.alanono.ditadotcc;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.alanono.ditadotcc.adapters.ResultadoResumoAdapter;
import br.com.alanono.ditadotcc.model.ResultadoResumo;
import butterknife.BindView;
import butterknife.ButterKnife;


public class ListarResultadosFragment extends Fragment {

    @BindView(R.id.listPalavras)
    ListView listView;

    List<ResultadoResumo> resumos = new ArrayList<ResultadoResumo>();

    ResultadoResumoAdapter adapter;

    public static ListarResultadosFragment newInstance() {
        ListarResultadosFragment fragment = new ListarResultadosFragment();
        return fragment;
    }

    public ListarResultadosFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_listar_resultados, container, false);
        ButterKnife.bind(this, view);
        BaseApp app = (BaseApp) getActivity().getApplication();
        String usuarioProf = app.getUsuarioProf();
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Carregando dados... aguarde");
        dialog.setIndeterminate(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();
        Rest.getInstance().getArray("/resultados/" + usuarioProf + "/all", new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(final JSONArray response) {
                Log.i("", response.toString());
                Gson gson = new Gson();
                resumos = Arrays.asList(gson.fromJson(response.toString(), ResultadoResumo[].class));
                adapter = new ResultadoResumoAdapter(getActivity(), resumos);
                listView.setAdapter(adapter);
                dialog.cancel();

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        if(adapter.getItem(i).getCount()> 0) {
                            String p = adapter.getItem(i).get_id();
                            getFragmentManager().beginTransaction().replace(R.id.fragment_container, ListarResultadosDetalhesFragment.newInstance(p)).addToBackStack(null).commit();
                        } else {
                            Toast.makeText(getActivity(), "Nenhum resultado dísponivel", Toast.LENGTH_LONG).show();
                        }
                    }
                });

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.cancel();
                Log.e("", error.getMessage(), error.fillInStackTrace());
                Toast.makeText(getActivity(), "Partida não encontrada", Toast.LENGTH_LONG).show();
            }
        });


        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }



}
