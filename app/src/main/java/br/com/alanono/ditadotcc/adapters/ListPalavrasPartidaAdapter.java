package br.com.alanono.ditadotcc.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.alanono.ditadotcc.R;
import br.com.alanono.ditadotcc.model.Palavra;


/**
 * Created by Alan on 10/10/2015.
 */
public class ListPalavrasPartidaAdapter extends ArrayAdapter<String> {
    List<String> palavras;
    private Context context;
    private LayoutInflater inflater;

    public ListPalavrasPartidaAdapter(Context context, List<String> palavras) {
        super(context, R.layout.row_palavra_partida, palavras);
        this.palavras = palavras;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.row_palavra_partida, parent, false);
        }

        String r = palavras.get(position);
        TextView palavra = (TextView) convertView.findViewById(R.id.palavra);
        ImageView excluir = (ImageView) convertView.findViewById(R.id.excluir);
        palavra.setText(r);

        excluir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                palavras.remove(position);
                notifyDataSetChanged();
            }
        });

        return convertView;
    }
}
