package br.com.alanono.ditadotcc;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by eusoj on 13/06/2017.
 */

public class Rest {
    private static Rest mInstance;
    private RequestQueue mRequestQueue;
    private static Context mCtx;

    private Rest(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized Rest getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new Rest(context);
        }
        return mInstance;
    }

    public static synchronized Rest getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        getRequestQueue().add(req);
    }

    public void deleteObject(String url, Response.Listener<JSONObject> success, Response.ErrorListener error){
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE, BuildConfig.SERVER_URL + url, null, success, error);
        addToRequestQueue(request);
    }
    public void getObject(String url, Response.Listener<JSONObject> success, Response.ErrorListener error){
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, BuildConfig.SERVER_URL + url, null, success, error);
        addToRequestQueue(request);
    }

    public void getArray(String url, Response.Listener<JSONArray> success, Response.ErrorListener error){
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, BuildConfig.SERVER_URL + url, null, success, error);
        addToRequestQueue(request);
    }

    public void doPost(String url, JSONObject req, Response.Listener<JSONObject> success, Response.ErrorListener error){
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, BuildConfig.SERVER_URL + url, req, success, error);
        addToRequestQueue(request);
    }

    public void updateContacts() {
        /*getObject("/contato", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(final JSONObject response) {
                Log.i(TAG, response.toString());
                Realm realm = Realm.getDefaultInstance();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        try {
                            response.getJSONArray("contatos");
                            realm.createOrUpdateAllFromJson(Contato.class, response.getJSONArray("contatos"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.getMessage(), error.fillInStackTrace());
            }
        });*/

    }

}