package br.com.alanono.ditadotcc;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.alanono.ditadotcc.adapters.ResultadoAdapter;
import br.com.alanono.ditadotcc.adapters.ResultadoDetalheAdapter;
import br.com.alanono.ditadotcc.model.Palavra;
import br.com.alanono.ditadotcc.model.ResultadoPartidaDTO;
import butterknife.BindView;
import butterknife.ButterKnife;


public class ListarResultadosFinalFragment extends Fragment {


    ResultadoPartidaDTO result;
    ResultadoAdapter resultadoAdapter;
    public static ListarResultadosFinalFragment newInstance(ResultadoPartidaDTO result) {
        ListarResultadosFinalFragment fragment = new ListarResultadosFinalFragment();
        fragment.result = result;
        return fragment;
    }

    public ListarResultadosFinalFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_result_final, container, false);

        BaseApp app = (BaseApp) getActivity().getApplication();

        List<Palavra> palavras = result.getPalavras();
        resultadoAdapter = new ResultadoAdapter(getActivity(), palavras);
        GridView gridView = (GridView) view.findViewById(R.id.grid_result);
        gridView.setAdapter(resultadoAdapter);

        TextView tempo = (TextView) view.findViewById(R.id.tempo);
        TextView acertos = (TextView) view.findViewById(R.id.qtdAcerto);

        SimpleDateFormat format = new SimpleDateFormat("mm:ss");

        tempo.setText(format.format(1L));
        acertos.setText(result.getAcertos() + "/" + (result.getAcertos() + result.getErros()));



        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }



}
