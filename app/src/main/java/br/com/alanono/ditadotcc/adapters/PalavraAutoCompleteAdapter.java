package br.com.alanono.ditadotcc.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.alanono.ditadotcc.R;



/**
 * Created by eusoj on 27/05/2017.
 */

public class PalavraAutoCompleteAdapter extends ArrayAdapter<String> {
    private final Context mContext;
    private final List<String> mStrings;
    private final List<String> mStrings_All;
    private final List<String> mStrings_Suggestion;
    private final int mLayoutResourceId;

    public PalavraAutoCompleteAdapter(Context context, int resource, List<String> palavras) {
        super(context, resource);
        this.mContext = context;
        this.mLayoutResourceId = resource;
        this.mStrings = new ArrayList<>();
        this.mStrings_All = palavras;
        this.mStrings_Suggestion = new ArrayList<>();
    }

    public int getCount() {
        return mStrings.size();
    }

    public String getItem(int position) {
        return mStrings.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            if (convertView == null) {
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(mLayoutResourceId, parent, false);
            }
            String p = getItem(position);
            TextView name = (TextView) convertView.findViewById(android.R.id.text1);
            name.setText(p);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            public String convertResultToString(Object resultValue) {
                if(resultValue != null){
                    String m = ((String) resultValue);
                    return m;
                }

                return "";
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if (constraint != null) {
                    mStrings_Suggestion.clear();
                    String c = constraint.toString().toUpperCase();

                    List<String> l = new ArrayList<>();
                    for(String s : mStrings_All){
                        if(s.startsWith(c))
                            l.add(s);
                    }

                    mStrings_Suggestion.addAll(l);
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = mStrings_Suggestion;
                    filterResults.count = mStrings_Suggestion.size();
                    return filterResults;
                } else {
                    return new FilterResults();
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mStrings.clear();
                if (results != null && results.count > 0) {
                    mStrings.addAll(mStrings_Suggestion);
                } else if (constraint == null) {
                    // no filter, add entire original list back in
                    mStrings.addAll(mStrings_All);
                }
                notifyDataSetChanged();
            }
        };
    }

    public static String capitalize(String source){
        String result = "";
        String[] splitString = source.split(" ");
        for(String target : splitString){
            result += Character.toUpperCase(target.charAt(0))
                    + target.substring(1) + " ";
        }
        return result.trim();
    }
}