package br.com.alanono.ditadotcc.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by eusoj on 07/03/2018.
 */

public class ResultadoPartidaDTO implements Serializable {
    String resultadoId;
    String partidaId;
    List<PalavraDTO> palavras;
    String usuario;
    Long acertos;
    Long erros;
    Long type;
    String prof;
    Long time;

    public String getResultadoId() {
        return resultadoId;
    }

    public void setResultadoId(String resultadoId) {
        this.resultadoId = resultadoId;
    }

    public String getPartidaId() {
        return partidaId;
    }

    public void setPartidaId(String partida) {
        this.partidaId = partida;
    }

    public List<PalavraDTO> getPalavrasDTO() {
        return palavras;
    }

    public void setPalavras(List<Palavra> palavras) {
        this.palavras = new ArrayList<>();
        for(Palavra p : palavras){
            this.palavras.add(new PalavraDTO(p));
        }
    }

    public List<Palavra> getPalavras() {
        List lp = new ArrayList<>();
        for(PalavraDTO p : this.palavras){
            lp.add(new Palavra(p));
        }
        return lp;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Long getAcertos() {
        return acertos;
    }

    public void setAcertos(Long acertos) {
        this.acertos = acertos;
    }

    public Long getErros() {
        return erros;
    }

    public void setErros(Long erros) {
        this.erros = erros;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getProf() {
        return prof;
    }

    public void setProf(String prof) {
        this.prof = prof;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
