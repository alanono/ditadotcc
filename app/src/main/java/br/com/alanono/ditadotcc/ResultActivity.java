package br.com.alanono.ditadotcc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import br.com.alanono.ditadotcc.adapters.ResultadoAdapter;
import br.com.alanono.ditadotcc.model.Palavra;
import br.com.alanono.ditadotcc.model.ResultadoPartidaDTO;

public class ResultActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        BaseApp app = (BaseApp) getApplication();
        List<Palavra> resultados = app.getResult();
        ResultadoAdapter resultadoAdapter = new ResultadoAdapter(this, resultados);
        GridView gridView = (GridView) findViewById(R.id.grid_result);
        gridView.setAdapter(resultadoAdapter);

        TextView tempo = (TextView) findViewById(R.id.tempo);
        TextView acertos = (TextView) findViewById(R.id.qtdAcerto);

        Long tempoAux = getIntent().getLongExtra(GameActivity.ARG_TEMPO, 0L);
        Long acertosAux = getIntent().getLongExtra(GameActivity.ARG_ACERTOS, 0L);
        Long errosAux = getIntent().getLongExtra(GameActivity.ARG_ERROS, 0L);
        String partida = getIntent().getStringExtra(GameActivity.ARG_PARTIDA);
        String prof = getIntent().getStringExtra(GameActivity.ARG_PROF);
        SimpleDateFormat format = new SimpleDateFormat("mm:ss");
        tempo.setText(format.format(new Date(tempoAux)));
        acertos.setText(acertosAux + "/" + (acertosAux + errosAux));

        if(partida != null){
            ResultadoPartidaDTO rp = new ResultadoPartidaDTO();
            rp.setAcertos(acertosAux);
            rp.setErros(errosAux);
            rp.setPalavras(resultados);
            rp.setPartidaId(partida);
            rp.setResultadoId(UUID.randomUUID().toString());
            rp.setUsuario(app.getUsuario());
            rp.setType(1L);
            rp.setTime(tempoAux);
            rp.setProf(prof);
            //envia resultado
            Gson gson = new Gson();
            String jsonInString = gson.toJson(rp);
            JSONObject mJSONObject = null;
            try {
                mJSONObject = new JSONObject(jsonInString);
                Rest.getInstance().doPost("/resultados", mJSONObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(ResultActivity.this, "Resultato enviado", Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ResultActivity.this, "Resultato NÃO enviado", Toast.LENGTH_LONG).show();
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_result, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void jogarNovamente(View view) {
        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra(GameActivity.ARG_DIFICULDADE, getIntent().getIntExtra(GameActivity.ARG_DIFICULDADE, 1));
        intent.putExtra(GameActivity.ARG_TAG, getIntent().getStringExtra(GameActivity.ARG_TAG));
        intent.putExtra(GameActivity.ARG_PARTIDA, getIntent().getStringExtra(GameActivity.ARG_PARTIDA));
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    public void openMenu(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

}
