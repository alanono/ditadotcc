package br.com.alanono.ditadotcc;

import android.app.Application;

import java.util.List;

import br.com.alanono.ditadotcc.model.Config;
import br.com.alanono.ditadotcc.model.Palavra;
import io.realm.Realm;
import io.realm.RealmQuery;

public class BaseApp extends Application {
    private List<Palavra> result;

    @Override
    public void onCreate() {
        super.onCreate();
        Rest.getInstance(this);
        Realm.init(this);
        InitialConfig.checkInicialConfigIsNeeded();
    }

    public List<Palavra> getResult() {
        return result;
    }

    public void setResult(List<Palavra> result) {
        this.result = result;
    }

    public String getUsuario(){
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Config> query = realm.where(Config.class);
        Config config = query.findFirst();
        if(config == null){
            config = new Config();
            config.setUsuario("");
            realm.beginTransaction();
            config = realm.copyToRealmOrUpdate(config);
            realm.commitTransaction();
        }
        return config.getUsuario();
    }

    public void setUsuario(String usuario){
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Config> query = realm.where(Config.class);
        Config config = query.findFirst();
        realm.beginTransaction();
        if(config == null){
            config = new Config();
            config.setUsuario("");
            config = realm.copyToRealmOrUpdate(config);
        }
        config.setUsuario(usuario);
        realm.commitTransaction();
    }

    public String getUsuarioProf(){
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Config> query = realm.where(Config.class);
        Config config = query.findFirst();
        if(config == null){
            config = new Config();
            config.setUsuarioProf("");
            realm.beginTransaction();
            config = realm.copyToRealmOrUpdate(config);
            realm.commitTransaction();
        }
        return config.getUsuarioProf();
    }

    public void setUsuarioProf(String usuario){
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Config> query = realm.where(Config.class);
        Config config = query.findFirst();
        realm.beginTransaction();
        if(config == null){
            config = new Config();
            config.setUsuarioProf("");
            config = realm.copyToRealmOrUpdate(config);
        }
        config.setUsuarioProf(usuario);
        realm.commitTransaction();
    }

    public void logout(){
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Config> query = realm.where(Config.class);
        Config config = query.findFirst();
        realm.beginTransaction();
        if(config == null){
            config = new Config();
        }
        config.setUsuarioProf("");
        config = realm.copyToRealmOrUpdate(config);
        realm.commitTransaction();
    }

}