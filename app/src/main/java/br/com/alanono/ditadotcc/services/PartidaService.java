package br.com.alanono.ditadotcc.services;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

import br.com.alanono.ditadotcc.Rest;
import br.com.alanono.ditadotcc.model.Config;
import br.com.alanono.ditadotcc.model.Palavra;
import br.com.alanono.ditadotcc.model.Partida;
import io.realm.Realm;
import io.realm.RealmQuery;

/**
 * Created by eusoj on 22/06/2018.
 */

public class PartidaService {
    String TAG = "PartidaService";


    public void getFromServer(String p, Response.Listener<JSONObject> suc, Response.ErrorListener err) {
        Rest.getInstance().getObject("/partidas/"+p, suc, err);
    }

    public Partida get(String p){
        final Realm realm = Realm.getDefaultInstance();
        RealmQuery<Partida> query = realm.where(Partida.class).equalTo("partidaId", p);
        return query.findFirst();

    }
}