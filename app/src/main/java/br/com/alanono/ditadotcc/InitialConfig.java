package br.com.alanono.ditadotcc;

import br.com.alanono.ditadotcc.model.Config;
import br.com.alanono.ditadotcc.model.Palavra;
import br.com.alanono.ditadotcc.model.Tag;
import br.com.alanono.ditadotcc.services.PalavrasService;
import io.realm.Realm;
import io.realm.RealmQuery;

/**
 * Created by eusoj on 07/03/2018.
 */

public class InitialConfig {
    public static void checkInicialConfigIsNeeded(){
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Config> query = realm.where(Config.class);
        Config config = query.findFirst();
        if(config == null ){
            doInitialInserts();
        }
    }

    private static void doInitialInserts() {
        new PalavrasService().sinc();
    }
}
