package br.com.alanono.ditadotcc;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.net.URLEncoder;

import br.com.alanono.ditadotcc.model.Partida;
import br.com.alanono.ditadotcc.services.PalavrasService;
import br.com.alanono.ditadotcc.services.PartidaService;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;


public class IniciarPartidaFragment extends Fragment {
    @BindView(R.id.partidaEt)
    EditText partidaEt;

    @BindView(R.id.nomeEt)
    EditText nomeEt;

    BaseApp app;

    final Realm realm = Realm.getDefaultInstance();

    public static String TAG = "IniciarPartidaFragment";

    public static IniciarPartidaFragment newInstance() {
        IniciarPartidaFragment fragment = new IniciarPartidaFragment();
        return fragment;
    }

    public IniciarPartidaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_iniciar_partida, container, false);
        ButterKnife.bind(this, view);
        app = (BaseApp) getActivity().getApplication();

        nomeEt.setText(app.getUsuario());
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @OnClick(R.id.iniciarPartida)
    public void iniciarPartida() {
        final String p = partidaEt.getText().toString();
        final String u = nomeEt.getText().toString();
        final String pp;
        if(u.isEmpty() || p.isEmpty()){
            Toast.makeText(getActivity(), "Favor preencher todos os campos", Toast.LENGTH_LONG).show();
            return;
        }

        pp = android.net.Uri.encode(p, "UTF-8");

        app.setUsuario(u);

        PalavrasService palavrasService = new PalavrasService();
        PartidaService partidaService = new PartidaService();
        partidaService.getFromServer(pp, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(final JSONObject response) {
                Log.i(TAG, response.toString());
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.createOrUpdateObjectFromJson(Partida.class, response);
                    }
                });
                Intent game = new Intent(getActivity(), GameActivity.class);
                game.putExtra(GameActivity.ARG_PARTIDA, p);
                game.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(game);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.getMessage(), error.fillInStackTrace());
                Toast.makeText(getActivity(), "Partida não encontrada", Toast.LENGTH_LONG).show();
            }
        });
    }
}
