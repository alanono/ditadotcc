package br.com.alanono.ditadotcc.model;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by eusoj on 07/03/2018.
 */

public class ResultadoPartida extends RealmObject {
    @PrimaryKey
    String resultadoId;
    String partida;
    RealmList<Palavra> palavras;
    String usuario;
    Long acertos;
    Long erros;

    public String getResultadoId() {
        return resultadoId;
    }

    public void setResultadoId(String resultadoId) {
        this.resultadoId = resultadoId;
    }

    public String getPartida() {
        return partida;
    }

    public void setPartida(String partida) {
        this.partida = partida;
    }

    public RealmList<Palavra> getPalavras() {
        return palavras;
    }

    public void setPalavras(RealmList<Palavra> palavras) {
        this.palavras = palavras;
    }
    public void setPalavras(List<Palavra> palavras) {
        this.palavras = new RealmList<>();
        for(Palavra p : palavras){
            this.palavras.add(p);
        }
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Long getAcertos() {
        return acertos;
    }

    public void setAcertos(Long acertos) {
        this.acertos = acertos;
    }

    public Long getErros() {
        return erros;
    }

    public void setErros(Long erros) {
        this.erros = erros;
    }
}
