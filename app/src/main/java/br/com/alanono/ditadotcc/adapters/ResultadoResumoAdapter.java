package br.com.alanono.ditadotcc.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import br.com.alanono.ditadotcc.R;
import br.com.alanono.ditadotcc.model.ResultadoResumo;


/**
 * Created by Alan on 10/10/2015.
 */
public class ResultadoResumoAdapter extends ArrayAdapter<ResultadoResumo> {
    List<ResultadoResumo> resultados;
    private Context context;
    private LayoutInflater inflater;

    public ResultadoResumoAdapter(Context context, List<ResultadoResumo> resultados) {
        super(context, R.layout.row_resultado_resumo, resultados);
        this.resultados = resultados;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.row_resultado_resumo, parent, false);
        }

        ResultadoResumo r = resultados.get(position);
        TextView partida = (TextView) convertView.findViewById(R.id.partida);
        TextView media = (TextView) convertView.findViewById(R.id.media);
        TextView total = (TextView) convertView.findViewById(R.id.total);

        partida.setText(r.get_id());
        total.setText("Quantidade de partidas: " + r.getCount());
        double m = r.getMediaAcertos();
        DecimalFormat df = new DecimalFormat("#.##");
        media.setText("Média de acertos: " + df.format(m) + "%");

        return convertView;
    }
}
