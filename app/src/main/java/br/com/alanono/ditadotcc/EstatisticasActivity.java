package br.com.alanono.ditadotcc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import br.com.alanono.ditadotcc.adapters.EstatisticasAdapter;
import br.com.alanono.ditadotcc.model.Config;
import br.com.alanono.ditadotcc.model.Palavra;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class EstatisticasActivity extends Activity {

    List<Palavra> palavras;
    LinearLayout estLayout;
    ProgressBar progressBar;
    Button jogarButton;
    TextView zeroResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estatistica);

        TextView tempo = (TextView) findViewById(R.id.tempo);
        TextView acertos = (TextView) findViewById(R.id.qtdAcerto);


        zeroResult = (TextView) findViewById(R.id.zeroResult);
        jogarButton = (Button) findViewById(R.id.buttonJogar);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        estLayout = (LinearLayout) findViewById(R.id.estLayout);

        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Palavra> query = realm.where(Palavra.class).greaterThan("erros", 0).sort("score");
        RealmResults<Palavra> palavrasAll = query.findAll();
        palavras = palavrasAll.subList(0, palavrasAll.size()<20 ? palavrasAll.size() : 20);


        if (palavras.size() > 0) {
            EstatisticasAdapter estatisticasAdapter = new EstatisticasAdapter(getBaseContext(), palavras);
            GridView gridView = (GridView) findViewById(R.id.grid_estatistica);
            gridView.setAdapter(estatisticasAdapter);

            if (palavras.size() < 5) {
                jogarButton.setVisibility(View.GONE);
            }
        } else {
            estLayout.setVisibility(View.GONE);
            zeroResult.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_result, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    public void jogarMaisErradas(View view) {
        Intent game = new Intent(getApplicationContext(), GameActivity.class);
        game.putExtra(GameActivity.ARG_DIFICULDADE, "-1");
        game.putExtra(GameActivity.ARG_TAG, GameActivity.MAIS_ERRADAS);
        game.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(game);

    }

}
