package br.com.alanono.ditadotcc.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import br.com.alanono.ditadotcc.R;
import br.com.alanono.ditadotcc.model.ResultadoPartidaDTO;
import br.com.alanono.ditadotcc.model.ResultadoResumo;


/**
 * Created by Alan on 10/10/2015.
 */
public class ResultadoDetalheAdapter extends ArrayAdapter<ResultadoPartidaDTO> {
    List<ResultadoPartidaDTO> resultados;
    private Context context;
    private LayoutInflater inflater;

    public ResultadoDetalheAdapter(Context context, List<ResultadoPartidaDTO> resultados) {
        super(context, R.layout.row_resultado_resumo, resultados);
        this.resultados = resultados;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.row_resultado_resumo, parent, false);
        }

        ResultadoPartidaDTO r = resultados.get(position);
        TextView partida = (TextView) convertView.findViewById(R.id.partida);
        TextView media = (TextView) convertView.findViewById(R.id.media);
        TextView total = (TextView) convertView.findViewById(R.id.total);

        partida.setText(r.getUsuario());
        total.setText("Acertos: " + r.getAcertos());
        media.setText("Erros: " + r.getErros());

        return convertView;
    }
}
